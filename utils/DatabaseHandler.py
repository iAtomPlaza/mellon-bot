import json


class DatabaseHandler:

    @staticmethod
    def open(path):
        with open(path, 'r') as file:
            data_base = json.load(file)

        return data_base

    @staticmethod
    def save(path, db):
        with open(path, 'w') as file:
            json.dump(db, file)

    @staticmethod
    def generate_base(path, file, ctx):
        if not str(ctx.message.guild.id) in file:
            file[str(ctx.message.guild.id)] = {}
            file[str(ctx.message.guild.id)]["bot_enabled"] = True
            file[str(ctx.message.guild.id)]["lang"] = 'en'
            file[str(ctx.message.guild.id)]["channel"] = 0
            file[str(ctx.message.guild.id)]["message"] = 0
            file[str(ctx.message.guild.id)]["image"] = None

            DatabaseHandler.save(path, file)
