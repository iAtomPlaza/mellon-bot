import discord
from discord.ext import commands
import asyncio
from utils.DatabaseHandler import DatabaseHandler
from itertools import cycle

client = commands.Bot(command_prefix=commands.when_mentioned_or("."))
client.remove_command("help")

client.wdb = r"config/wdb.json"
client.images = "images/"

raw_token = DatabaseHandler.open(r"token.json")
token = raw_token["token"]

invite_Link = "https://discordapp.com/api/oauth2/authorize?client_id=507224478019747851&permissions=8&scope=bot"

server_count = []
member_count = []
client.member_count = []

client.in_setup_mode = []

info = "[Server thread/INFO]:"
notice = "[Server thread/NOTICE]:"
warning = "[Server thread/WARNING]:"

extensions = [
    # Events
    "events.Join",

    # Commands
    "commands.Members",
    "commands.Help",
    "commands.Info",
    "commands.Ping",
    "commands.setup.Channel",
    "commands.setup.Message",
    "commands.setup.Image"
]


@client.event
async def on_ready():
    print(info + " Preparing Bot Extensions!\n")

    if __name__ == "__main__":
        for extension in extensions:
            try:
                client.load_extension(extension)
                print(notice + " Loaded {} Successfully".format(extension))
            except Exception as error:
                print(warning + " {} Could not be loaded: [{}]".format(extension, error))

    for server in client.guilds:
        server_count.append(server)

        for member in server.members:
            member_count.append(member)

    print("\n" + info + " Bot is online in {} Server(s)".format(len(server_count)))
    print(info + " Serving {} members across Discord".format(len(member_count)))
    await change_status()


# @client.event
# async def on_command_error(error):
#     if isinstance(error, commands.CommandNotFound):
#         return


@client.event
async def on_message(message):
    try:
        await client.process_commands(message)
    except discord.ext.commands.errors.CommandNotFound:
        return


# Changes the bots profile status
async def change_status():
    status_messages = [
        "{} members".format(len(member_count)),
        ".help command! <3"
    ]

    messages = cycle(status_messages)

    while not client.is_closed():
        msg = next(messages)
        await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=msg),
                                     status=discord.Status.dnd, afk=False)
        await asyncio.sleep(5)


while True:
    try:
        client.loop.run_until_complete(client.start(token))
    except KeyboardInterrupt:
        client.loop.run_until_complete(client.logout())
    finally:
        client.loop.close()
