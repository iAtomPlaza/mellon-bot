# ![logo](images/bot.utils/logo-32x32.png) Mellon Bot
Not a multi function discord bot! only has one function.

Don't want Mellon-Bot eating your cpu or ram!? [Invite already hosted bot by me!](https://discordapp.com/oauth2/authorize?client_id=547913133990215705&permissions=8&scope=bot)

# Discord bot abilities...
Mellon-Bot grabs a NEW users avatar or profile image and pastes it onto a background banner image for a cool aesthetic welcome message to any new member!

# Commands
| Prefix | Command | Usage    | Description |
| ------ | ------- | -------- | ----------- |
| .      | help    | .help    | Shows all commands |
| .      | channel | .channel | Set the channel to use |
| .      | message | .message | Set the welcome message |
| .      | image   | .image   | Adds an image to use |

# Images
You can use default image provided or upload your own;

Images must be in `1920x1080` resolution.

# tags
You can use tags when setting message:

| Tag | Replaced with |
| --- | ------------- |
| SERVER | your discord server name |
| MEMBER | new members username |
| MENTION | mentions new user |
| NUMBER | new users membercount |

### Example

![logo](images/bot.utils/example.png)

