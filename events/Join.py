import discord
from discord.ext import commands
from utils.DatabaseHandler import DatabaseHandler
import aiohttp
from PIL import Image, ImageDraw
from io import BytesIO


class Join(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_member_join(self, member):
        wdb = DatabaseHandler.open(self.client.wdb)
        path = wdb[str(member.guild.id)]["image"]
        ch_id = wdb[str(member.guild.id)]["channel"]
        channel = member.guild.get_channel(int(ch_id))
        msg = wdb[str(member.guild.id)]["message"]
        message = msg \
            .replace("MENTION", member.mention) \
            .replace("MEMBER", member.name) \
            .replace("SERVER", member.guild.name) \
            .replace("NUMBER", str(member.guild.member_count))

        if wdb[str(member.guild.id)]["image"] == 0:
            await channel.send(message)
            return
        if wdb[str(member.guild.id)]["image"] == 1:
            if not member.avatar_url == "":
                avatar = member.avatar_url
            else:
                avatar = member.default_avatar_url

            async with aiohttp.ClientSession() as session:
                async with session.get(str(avatar)) as image:
                    image = await image.read()

                with Image.open(BytesIO(image)) as pfp:
                    buffer = BytesIO()
                    pfp = pfp.resize((400, 400))
                    size = (pfp.size[0] * 3, pfp.size[1] * 3)
                    mask = Image.new('L', size, 0)
                    draw = ImageDraw.Draw(mask)
                    draw.ellipse((0, 0) + size, fill=255)
                    mask = mask.resize(pfp.size, Image.ANTIALIAS)
                    pfp.putalpha(mask)

                    bk = Image.open(self.client.images + 'bot.default/default_1.png')
                    # bk = Image.open(self.client.images + 'user.upload/' + str(member.guild.id) + '.png')

                    bk.paste(pfp, (710, 340), pfp)
                    bk.save(buffer, 'png')
                    buffer.seek(0)

                file = discord.File(fp=buffer, filename=member.name + ".png")
                await channel.send(content=message, file=file)
            return
        if wdb[str(member.guild.id)]["image"] == 2:
            if not member.avatar_url == "":
                avatar = member.avatar_url
            else:
                avatar = member.default_avatar_url

            async with aiohttp.ClientSession() as session:
                async with session.get(str(avatar)) as image:
                    image = await image.read()

                with Image.open(BytesIO(image)) as pfp:
                    buffer = BytesIO()
                    pfp = pfp.resize((400, 400))
                    size = (pfp.size[0] * 3, pfp.size[1] * 3)
                    mask = Image.new('L', size, 0)
                    draw = ImageDraw.Draw(mask)
                    draw.ellipse((0, 0) + size, fill=255)
                    mask = mask.resize(pfp.size, Image.ANTIALIAS)
                    pfp.putalpha(mask)

                    # bk = Image.open(self.client.images + 'bot.default/default_1.png')
                    bk = Image.open(self.client.images + 'user.upload/' + str(member.guild.id) + '.png')

                    bk.paste(pfp, (710, 340), pfp)
                    bk.save(buffer, 'png')
                    buffer.seek(0)

                file = discord.File(fp=buffer, filename=member.name+".png")
                await channel.send(content=message, file=file)
                return


def setup(client):
    client.add_cog(Join(client))
