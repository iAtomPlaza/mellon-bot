import discord
from discord.ext import commands


class Help(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True)
    async def help(self, ctx):
        embed = discord.Embed(
            color=0x36393E
        )

        embed.set_author(
            name="{} | Server help page".format(ctx.message.author),
            icon_url=ctx.message.author.avatar_url)

        embed.add_field(
            name=".channel",
            value="set channel", inline=True)

        embed.add_field(
            name=".message",
            value="set message", inline=True)

        embed.add_field(
            name=".image",
            value="set image", inline=True)

        embed.set_footer(
            text="{} | {}".format(ctx.message.guild.name, ctx.message.created_at),
            icon_url=ctx.message.guild.icon_url)

        await ctx.message.channel.send(embed=embed)


def setup(client):
    client.add_cog(Help(client))
