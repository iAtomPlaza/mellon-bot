import discord
from discord.ext import commands


class Members(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True)
    async def members(self, ctx):

        count = []
        for server in self.client.guilds:
            for member in server.members:
                count.append(member)

        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name="{}".format(ctx.message.author), icon_url=ctx.message.author.avatar_url)
        embed.add_field(name="Total Membercount", value=ctx.message.guild.member_count, inline=True)
        embed.set_footer(text="{} | {}".format(ctx.message.guild.name, ctx.message.created_at),
                         icon_url=ctx.message.guild.icon_url)

        await ctx.message.channel.send(embed=embed)


def setup(client):
    client.add_cog(Members(client))
