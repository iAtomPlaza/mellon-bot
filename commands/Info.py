import discord
from discord.ext import commands


class Info(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True)
    async def info(self, ctx):
        embed = discord.Embed(
            color=0x36393E
        )

        embed.set_author(
            name="Bot Info",
            icon_url=ctx.message.author.avatar_url)

        embed.add_field(
            name="Bot Developer",
            value="<@287682736104275968>", inline=True)

        embed.add_field(
            name="`",
            value="[Support Server](https://discord.gg/wD6BuEn)", inline=True)

        embed.add_field(
            name="`",
            value="[Invite Me]" +
                  "(https://discordapp.com/api/oauth2/authorize?client_id=547913133990215705&permissions=8&scope=bot)",
            inline=True)

        embed.set_footer(
            text="{} | {}".format(ctx.message.guild.name, ctx.message.created_at),
            icon_url=ctx.message.guild.icon_url)

        await ctx.message.channel.send(embed=embed)


def setup(client):
    client.add_cog(Info(client))
