from discord.ext import commands
from utils.DatabaseHandler import DatabaseHandler
import asyncio


class Message(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True)
    async def message(self, ctx):
        if ctx.message.author.guild_permissions.administrator:
            await init(self, ctx)
        else:
            await ctx.message.channel.send("You do not have permission to run this command")


# registers class
def setup(client):
    client.add_cog(Message(client))


async def init(self, ctx):
    # open database for reading data
    wdb = DatabaseHandler.open(self.client.wdb)

    # generate db file if not exist
    if not str(ctx.message.guild.id) in wdb:
        DatabaseHandler.generate_base(self.client.wdb, wdb, ctx)

    await ctx.message.channel.send("Please type message to be displayed when a new user joins!")

    def wait_for_message(m):
        if not m.author.bot:
            msg = m.content
            wdb[str(m.guild.id)]["message"] = msg
            return m.content == msg

    try:
        await self.client.wait_for('message', timeout=20.0, check=wait_for_message)

        # saves database
        DatabaseHandler.save(self.client.wdb, wdb)

        # confirm message
        message = wdb[str(ctx.message.guild.id)]["message"]\
            .replace("MENTION", ctx.message.author.mention)\
            .replace("MEMBER", ctx.message.author.name)\
            .replace("SERVER", ctx.message.guild.name)\
            .replace("NUMBER", str(ctx.message.guild.member_count))

        await ctx.message.channel.send("Message set as \n" + message)

        if wdb[str(ctx.message.guild.id)]["channel"] == 0:
            await ctx.message.channel.send("Message not setup yet; run `.message` to set message")
            return
        elif wdb[str(ctx.message.guild.id)]["image"] is None:
            await ctx.message.channel.send("Welcome images not setup yet; "
                                           "you can choose to opt-out of using images by simply skipping this step; "
                                           "run `.image` to setup images")

    except asyncio.TimeoutError:
        await ctx.message.channel.send("timed out! try again...")
