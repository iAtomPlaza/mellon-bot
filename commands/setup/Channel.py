from discord.ext import commands
from utils.DatabaseHandler import DatabaseHandler
import asyncio


class Channel(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True)
    async def channel(self, ctx):
        if ctx.message.author.guild_permissions.administrator:
            await init(self, ctx)
        else:
            await ctx.message.channel.send("You do not have permission to run this command")


# registers class
def setup(client):
    client.add_cog(Channel(client))


async def init(self, ctx):
    # open database for reading data
    wdb = DatabaseHandler.open(self.client.wdb)

    # generate db file if not exist
    if not str(ctx.message.guild.id) in wdb:
        DatabaseHandler.generate_base(self.client.wdb, wdb, ctx)

    await ctx.message.channel.send("Please #mention the channel you with to use!")

    def wait_for_channel(m):
        if len(m.channel_mentions) == 0:
            return
        else:
            cid = m.channel_mentions[0].id
            channel = m.guild.get_channel(int(cid)).mention
            wdb[str(m.guild.id)]["channel"] = cid

        return m.content == channel

    try:
        await self.client.wait_for('message', timeout=10.0, check=wait_for_channel)

        # saves database
        DatabaseHandler.save(self.client.wdb, wdb)

        # confirm channel
        c_id = wdb[str(ctx.message.guild.id)]["channel"]
        ch_obj = ctx.message.guild.get_channel(int(c_id))
        await ctx.message.channel.send("Channel set as " + ch_obj.mention)

        if wdb[str(ctx.message.guild.id)]["message"] == 0:
            await ctx.message.channel.send("Message not setup yet; run `.message` to set message")
            return
        elif wdb[str(ctx.message.guild.id)]["image"] is None:
            await ctx.message.channel.send("Welcome images not setup yet; "
                                           "you can choose to opt-out of using images by simply skipping this step; "
                                           "run `.image` to setup images")
            return

    except asyncio.TimeoutError:
        await ctx.message.channel.send("timed out! try again...")
