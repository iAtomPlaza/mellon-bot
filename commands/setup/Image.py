from discord.ext import commands
from utils.DatabaseHandler import DatabaseHandler
import asyncio
import discord
import aiohttp
from PIL import Image as img, ImageDraw
from io import BytesIO


class Image(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True)
    async def image(self, ctx):
        if ctx.message.author.guild_permissions.administrator:
            await init(self, ctx)
        else:
            await ctx.message.channel.send("You do not have permission to run this command")

    @commands.Cog.listener()
    async def on_message(self, message):
        if not message.author.bot and message.author.guild_permissions.administrator:
            if str(message.guild.id) in self.client.in_setup_mode:
                if len(message.attachments) != 0:
                    att = message.attachments[0]
                    path = self.client.images + 'user.upload/' + str(message.guild.id) + '.png'
                    await att.save(fp=path)
                    await send_test_message(self, message)
                    self.client.in_setup_mode.remove(str(message.guild.id))


# registers class
def setup(client):
    client.add_cog(Image(client))


async def init(self, ctx):
    self.client.in_setup_mode.append(str(ctx.message.guild.id))

    # open database for reading data
    wdb = DatabaseHandler.open(self.client.wdb)

    # generate db file if not exist
    if not str(ctx.message.guild.id) in wdb:
        DatabaseHandler.generate_base(self.client.wdb, wdb, ctx)

    await ctx.message.channel.send("`0` for no image\n"
                                   "`1` for default image\n"
                                   "or upload image here: -- must be __**1920x1080**__")

    def wait_for_image(m):
        if m.content == '0':
            wdb[str(m.guild.id)]["image"] = 0
            return m.content == '0'
        elif m.content == '1':
            wdb[str(m.guild.id)]["image"] = 1
            return m.content == '1'
        elif m.content == '':
            wdb[str(m.guild.id)]["image"] = 2
            return m.content == ''

    try:
        await self.client.wait_for('message', timeout=20.0, check=wait_for_image)

        # saves database
        DatabaseHandler.save(self.client.wdb, wdb)

        # confirm message
        image = wdb[str(ctx.message.guild.id)]["image"]
        if image == 0:
            await ctx.message.channel.send("Not using Image")
            return

        if image == 1:
            await ctx.message.channel.send("Using Default Image")
            # await send_test_message(self, ctx)
            return

        if image == 2:
            await ctx.message.channel.send("Using Custom Image; Setup complete!")
            # await send_test_message(self, ctx)
            return

    except asyncio.TimeoutError:
        await ctx.message.channel.send("timed out! try again...")


async def send_test_message(self, message):
    member = message.author
    wdb = DatabaseHandler.open(self.client.wdb)
    path = wdb[str(member.guild.id)]["image"]
    ch_id = wdb[str(member.guild.id)]["channel"]
    channel = member.guild.get_channel(int(ch_id))
    msg = wdb[str(member.guild.id)]["message"]
    message = msg \
        .replace("MENTION", member.mention) \
        .replace("MEMBER", member.name) \
        .replace("SERVER", member.guild.name) \
        .replace("NUMBER", str(member.guild.member_count))

    if wdb[str(member.guild.id)]["image"] == 2:
        if not member.avatar_url == "":
            avatar = member.avatar_url
        else:
            avatar = member.default_avatar_url

        async with aiohttp.ClientSession() as session:
            async with session.get(str(avatar)) as image:
                image = await image.read()

            with img.open(BytesIO(image)) as pfp:
                buffer = BytesIO()
                pfp = pfp.resize((400, 400))
                size = (pfp.size[0] * 3, pfp.size[1] * 3)
                mask = img.new('L', size, 0)
                draw = ImageDraw.Draw(mask)
                draw.ellipse((0, 0) + size, fill=255)
                mask = mask.resize(pfp.size, img.ANTIALIAS)
                pfp.putalpha(mask)

                bk = img.open(self.client.images + 'user.upload/' + str(member.guild.id) + '.png')

                bk.paste(pfp, (710, 340), pfp)
                bk.save(buffer, 'png')
                buffer.seek(0)

            file = discord.File(fp=buffer, filename=member.name + ".png")
            await channel.send(content=message, file=file)

    elif wdb[str(member.guild.id)]["image"] == 1:
        if not member.avatar_url == "":
            avatar = member.avatar_url
        else:
            avatar = member.default_avatar_url

        async with aiohttp.ClientSession() as session:
            async with session.get(str(avatar)) as image:
                image = await image.read()

            with img.open(BytesIO(image)) as pfp:
                buffer = BytesIO()
                pfp = pfp.resize((400, 400))
                size = (pfp.size[0] * 3, pfp.size[1] * 3)
                mask = img.new('L', size, 0)
                draw = ImageDraw.Draw(mask)
                draw.ellipse((0, 0) + size, fill=255)
                mask = mask.resize(pfp.size, img.ANTIALIAS)
                pfp.putalpha(mask)

                bk = img.open(self.client.images + 'bot.default/default_1.png')

                bk.paste(pfp, (710, 340), pfp)
                bk.save(buffer, 'png')
                buffer.seek(0)

            file = discord.File(fp=buffer, filename=member.name + ".png")
            await channel.send(content=message, file=file)
